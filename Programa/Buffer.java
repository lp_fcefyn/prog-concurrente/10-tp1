import java.util.*; 

public class Buffer {
	private int max; 
	private LinkedList<Integer> list;
	private int LugaresDisponibles;
	private int DatosPerdidos = 0, Datosingresados = 0, DatosConsumidos = 0, DatosNoConsumidos = 0;
	private Object lock;
	private boolean buffer_terminar_proceso = false;
	
	public Buffer(int m) 
	{
		this.max = m;
		this.list = new LinkedList<Integer>();
		this.LugaresDisponibles = 10 - list.size();
		this.lock = new Object();
	}
	
	public void consumir()
	{
			list.remove();
			this.LugaresDisponibles = 10 - list.size();
	}
	
	public void producir()
	{
		synchronized(lock)
		{
				if(!(Datosingresados == this.max))
				{
					if(this.list.size() < 10)
					{
						list.add(1);
						this.Datosingresados++;
						System.out.println("Procesando dato "+this.Datosingresados+" de "+this.max);
						lock.notifyAll();	// Si se durmieron consumidores porque no hab�a para consumir, ahora los despierto
					}
					else			
					{
						this.DatosPerdidos++;
						this.Datosingresados++;
						System.out.println("Procesando dato "+this.Datosingresados+" de "+this.max);
						// Este wait incrementa la probabilidad de que un consumidor ocupe el synchronized, duermo los productores porque
						try {	// el buffer ya se encuentra lleno y no hay necesidad de seguir produciendo
							lock.wait();
						} catch (InterruptedException e) {}
					}
					if(Datosingresados == this.max)
					{
						this.buffer_terminar_proceso = true;
						try
						{
							lock.notifyAll();
						}catch (Exception e) {}
					}
					this.LugaresDisponibles = 10 - list.size();
				}
		}
	}
	
	public int lDisp()
	{
		return this.LugaresDisponibles;
	}
	
	public Object getLock()
	{
		return this.lock;
	}
	
	public LinkedList<Integer> getList()
	{
		return this.list;
	}
	
	public boolean Buffer_end()
	{
		return this.buffer_terminar_proceso;
	}
	
	public int getProducidos()
	{
		return this.Datosingresados;
	}
	public int getPerdidos()
	{
		return this.DatosPerdidos;
	}
	
	public void IncConsumidos()
	{
		this.DatosConsumidos++;
	}
	
	public int getConsumidos()
	{
		return this.DatosConsumidos;
	}
	
	public void IncNoConsumidos()
	{
		this.DatosNoConsumidos++;
	}
	
	public int getNoConsumidos()
	{
		return this.DatosNoConsumidos;
	}
	
	public int getMax()
	{
		return this.max;
	}
	
	public void setEnd(boolean b)
	{
		this.buffer_terminar_proceso = b;
	}
}
