
public class Consumidor implements Runnable{
	private int id;
	private Buffer b;
	private int alpha,beta;
	private int t_dormido;
	private Log l;
	
	public Consumidor(int id, Buffer b, Log l) 
	{
		this.id = id;
		this.b = b;
		this.l = l;
		this.alpha = 50;
		this.beta = 100;
	}

	@Override
	public void run() 
	{
		while(!this.b.Buffer_end())
		{
			//System.out.println("Estoy en el while del consumidor "+this.id);
			this.l.getConsumerStateList()[this.id] = "Disponible";
			synchronized(this.b.getLock())
			{
				if(this.b.getProducidos() == this.b.getMax())
				{
					try
					{
						this.b.getLock().notifyAll();
					}catch (Exception e) {}
				}
				else
					if(b.getList().size() > 0)
					{
						this.l.getConsumerStateList()[this.id] = "Ocupado consumiendo";
						this.b.consumir();
						this.b.IncConsumidos();
					}
					else	
					{		
						this.b.IncNoConsumidos();
						this.l.getConsumerStateList()[this.id] = "Disponible";
						// 
						this.b.getLock().notifyAll();	// si se duermieron productores por no haber lugar en el buffer, ahora despiertense
						try {
							this.b.getLock().wait();	// No hay elementos en el buffer, duermo al consumidor hasta que alguien le avise que
						} catch (InterruptedException e) {}	// ya se puede consumir nuevamente
					}
			}
			if(this.l.getConsumerStateList()[this.id].equals("Ocupado consumiendo"))
			{
				this.t_dormido = ((int) (Math.random() * (this.beta - this.alpha)) + this.alpha);
				try {
					Thread.sleep(t_dormido);
				} catch (InterruptedException e) {Thread.currentThread().interrupt();}
				//System.out.println("Soy el consumidor "+this.id+" y he dormido "+this.t_dormido+" milisegundos");
			}
			this.l.getConsumerStateList()[this.id] = "Disponible";
		}
	}
	
}
