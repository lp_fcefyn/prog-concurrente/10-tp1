import java.io.PrintWriter;

public class Log implements Runnable{
	private int cantConsumidores;
	private String[] estadoConsumidores;
	private int nReporte = 0;
	private Buffer b;
    private PrintWriter pw;
    private int Ocupados = 0;
    private int Disponibles = 0;
    private boolean salir = false;
	
	public Log(int c, Buffer b, PrintWriter pw) 
	{
		this.cantConsumidores = c;
		this.estadoConsumidores = new String[cantConsumidores];
		for(int i = 0; i < this.cantConsumidores; i++)
		{
			this.estadoConsumidores[i]= "Sin datos";
		}
		this.b = b;
        this.pw = pw;
	}
	
	public String[] getConsumerStateList()
	{
		return this.estadoConsumidores;
	}

	@Override
	public void run() {
		//while(!this.b.Buffer_end())
		while(!salir)
		{
			if(b.Buffer_end())
			{
				salir = true;
			}
			else
			{
				this.nReporte++;
				pw.printf("Log %d: \n" , nReporte);
	            for(int i = 0; i < this.cantConsumidores; i++)
	            {
	            	pw.printf("Consumidor %d %s \n",i,this.estadoConsumidores[i]);
	            	//System.out.println("Entre al log");
	            	if(this.estadoConsumidores[i].equals("Ocupado consumiendo"))
	            	{
	            		this.Ocupados++;
	            	}
	            	if(this.estadoConsumidores[i].equals("Disponible"))
	            	{
	            		this.Disponibles++;
	            	}
	            }
	            pw.printf("\n");
	            try {
	            	Thread.sleep(200);
	            } catch (InterruptedException ex) {}
			}	
		}
		pw.close();
		System.out.println("Log ha terminado, captando "+this.Disponibles+" veces consumidores Disponibles y "+this.Ocupados+
				" veces consumidores Ocupados");
	}
	
}
