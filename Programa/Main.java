import java.io.FileWriter;
import java.io.PrintWriter;

public class Main {
	public static void main(String[] args)
	{
		int MaxProductos = 1000;
		int cantProductores = 8;
		int cantConsumidores = 2;
		Buffer b = new Buffer(MaxProductos);
		
		FileWriter fichero = null;
	    PrintWriter pw = null;

	    try
        {
            fichero = new FileWriter("log.txt");
            pw = new PrintWriter(fichero);
        } catch (Exception e) {}
	    
	    Log l = new Log(cantConsumidores,b,pw);
		
		// Cuidado, las impresiones por consola modifican el comportamiento del sistema, ya que demoran un cierto tiempo
		
		Thread hiloLog = new Thread(l);
		Thread c[] = new Thread[cantConsumidores];
		Thread p[] = new Thread[cantProductores];
		
		for(int i = 0; i < cantProductores; i++)
		{
			p[i] = new Thread(new Productor(b));
			p[i].start();
		}
		for(int i = 0; i < cantConsumidores; i++)
		{
			c[i] = new Thread(new Consumidor(i,b,l));
			c[i].start();
		}
		
		hiloLog.start();
		
		for(int i = 0; i < cantProductores; i++)
		{
			try {
				p[i].join();
			} catch (InterruptedException e) {}
		}
		
		for(int i = 0; i < cantConsumidores; i++)
		{
			try {
				c[i].join();
			} catch (InterruptedException e) {}
		}
		try {
			hiloLog.join();
		} catch (InterruptedException e) {}
	
		System.out.println("Soy el hilo principal, terminaron las ejecuciones, se han introducido "+b.getProducidos()+
				" productos, se perdieron "+b.getPerdidos()+", se consumieron "+b.getConsumidos()+" y no se pudo consumir "+
				b.getNoConsumidos()+" veces. El buffer tiene en este momento "+b.getList().size()+" elementos");
	
	}
}
