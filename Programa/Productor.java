
public class Productor implements Runnable{
	private Buffer b;
	
	public Productor(Buffer b) 
	{
		this.b = b;
	}

	@Override
	public void run() {
		while(!this.b.Buffer_end())
		{
			this.b.producir();
		}
		try
		{
			this.b.getLock().notifyAll();
		}catch (Exception e) {}
	}
	
}
